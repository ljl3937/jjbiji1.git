import os
import string

# 获取当前目录下的所有文件和文件夹
entries = os.listdir('.')

# 排除.vuepress和node_modules文件夹
entries = [entry for entry in entries if entry not in [".vuepress", "node_modules"]]

# 遍历所有文件和文件夹
for entry in entries:
    if os.path.isdir(entry):
        # 生成对应的SideBar.ts文件名
        sidebar_filename = f".vuepress/sidebars/{string.capwords(entry.replace('-', ' ')).replace(' ', '')}SideBar.ts"
        
        # 检查文件是否已存在
        if os.path.exists(sidebar_filename):
            # 如果已存在，则清空文件内容
            with open(sidebar_filename, 'w') as f:
                f.write("")
        else:
            # 如果不存在，则创建新文件
            with open(sidebar_filename, 'w') as f:
                pass

        # 获取文件夹内的所有文件(含子目录)
        files = []
        for root, dirs, filenames in os.walk(entry):
            for filename in filenames:
                if filename != 'README.md':
                    file_path = os.path.join(root, filename)
                    # file_path中排除entry，并去掉前面的/
                    file_path = file_path.replace(entry, '')[1:]
                    files.append(file_path)
        print(entry)
        print(files)
        
        file_str = ""
        for file in files:
            file_str = f"{file_str}        \"{file}\",\n"
        # 生成文件内容
        content = f"export default [\n    \"\",\n    {{\n      title: \"{entry}\",\n      collapsable: true,\n      children: [\n{file_str}\n      ]\n    }},\n];\n"

        # 写入文件内容
        with open(sidebar_filename, 'w') as f:
            f.write(content)
