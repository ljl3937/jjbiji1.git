export default [
    "",
    {
      title: ".workflow",
      collapsable: true,
      children: [
        "branch-pipeline.yml",
        "pr-pipeline.yml",
        "master-pipeline.yml",

      ]
    },
];
