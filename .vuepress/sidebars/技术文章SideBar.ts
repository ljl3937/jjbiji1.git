export default [
    "",
    {
      title: "技术文章",
      collapsable: true,
      children: [
        "AI动态/全球首位AI程序员Devin的问世：编程领域的未来已来.md",
        "AI动态/kimi终于增加智能体功能了.md",
        "AI动态/震撼发布！Meta Llama 3惊艳登场.md",
        "模型设计/Agent工作流的四种关键设计思维模式.md",
        "模型设计/AI时代下的思维革新：底层思维与顶层框架.md",

      ]
    },
];
