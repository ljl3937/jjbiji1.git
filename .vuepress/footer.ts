/**
 * 底部版权信息
 */
export default {
  friendLinks: [
    {
      label: "站长 - 加加",
      // icon: "/icon/user.svg",
      href: "https://www.jjbiji.com",
    },
    {
      label: "AI成长路径",
      href: "https://jobplan.jjbiji.com/",
    },
    {
      label: "AI智能体编程之旅",
      href: "https://ffklj1ebum.feishu.cn/wiki/AhH2wpkD6iEG6gkwMfCcrBe4nHf?from=from_copylink",
    },
    {
      label: "盘古云",
      href: "http://cloud.panguidc.com/aff/LRJHVTYE",
    },
  ],
  copyright: {
    href: "https://beian.miit.gov.cn/",
    name: "豫ICP备15035550号-1",
  },
};
