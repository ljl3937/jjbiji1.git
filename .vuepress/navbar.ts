import {NavItem} from "vuepress/config";

export default [
    {
        text: "学习资源",
        link: '/学习资源/',
        items: [
            {
                text: "LangChain", link: "/学习资源/#LangChain",
            }
        ]
    },
    {
        text: "技术文章",
        link: "/技术文章/",
        items: [
            {
                text: "模型设计", link: "/技术文章/#模型设计",
            },
            {
                text: "AI 动态", link: "/技术文章/#AI动态",
            }
        ]
    },
    {
        text: "工具推荐",
        link: '/工具推荐/',
        items: [
            {
                text: "辅助开发", link: "/工具推荐/#辅助开发",
            },




        ]
    },
    {
        text: "项目实战",
        link: '/项目实战/'
    },
    {
        text: "专家专栏",
        link: '/专家专栏/'
    },
    {
        text: "关于作者",
        link: '/关于作者/'
    },
] as NavItem[];
