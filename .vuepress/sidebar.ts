import {SidebarConfig4Multiple} from "vuepress/config";
import 学习资源SideBar from "./sidebars/学习资源SideBar";
import 技术文章SideBar from "./sidebars/技术文章SideBar";
import 项目实战SideBar from "./sidebars/项目实战SideBar";
import 工具推荐SideBar from "./sidebars/工具推荐SideBar";
import 专家专栏SideBar from "./sidebars/专家专栏SideBar";
import 关于作者SideBar from "./sidebars/关于作者SideBar";
// @ts-ignore
export default {
    "/学习资源/": 学习资源SideBar,
    "/技术文章/": 技术文章SideBar,
    "/工具推荐/": 工具推荐SideBar,
    "/项目实战/": 项目实战SideBar,
    "/专家专栏/": 专家专栏SideBar,
    "/关于作者/": 关于作者SideBar,
    // "/关于我们/": ["", "个人经历"],
    // 降级，默认根据文章标题渲染侧边栏
    "/": "auto",
} as SidebarConfig4Multiple;
