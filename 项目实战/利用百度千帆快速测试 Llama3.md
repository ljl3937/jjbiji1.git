# 百度千帆快速测试 Llama3

百度千帆大模型平台太给力了！昨晚Mata 刚刚更新的Llama3，今天我就收到了来自百度千帆的短信通知，让我可以在千帆大模型平台使用Llama3了。我也很激动，有如此方便的体验方式，我怎么能不赶快试一把呢？于是，我立即操练起来！

![20240419214556](https://jjbiji-pic.oss-cn-beijing.aliyuncs.com/pic20240419214556.png)
虽说Llama3模型是开源的，但是百度的千帆平台的收费并不贵，而且提供了更方便的体验方式，我只需要在平台上注册一个账号，然后选择Llama3模型，就可以直接使用Llama3模型了。然而，70B的模型就显得贵了一些。

![20240419215830](https://jjbiji-pic.oss-cn-beijing.aliyuncs.com/pic20240419215830.png)
![20240419214219](https://jjbiji-pic.oss-cn-beijing.aliyuncs.com/pic20240419214219.png)

体验下来，虽然Llama3模型支持中文，但是它的回答好像默认还是英文的啊。
![20240419215105](https://jjbiji-pic.oss-cn-beijing.aliyuncs.com/pic20240419215105.png)
![20240419220233](https://jjbiji-pic.oss-cn-beijing.aliyuncs.com/pic20240419220233.png)