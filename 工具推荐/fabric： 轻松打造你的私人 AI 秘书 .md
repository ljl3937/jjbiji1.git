#  fabric： 轻松打造你的私人 AI 秘书 

原文：https://mp.weixin.qq.com/s/zLJ-50rbxk3T8DZX-2F_7A

图片

回想起 ChatGPT 刚出道的时候，着实惊艳了我们的下巴，但是随着大家对它的越来越了解，知道要用好它，还是需要一些 prompt 技巧的，所以，有些人说，未来可能会诞生一批人，这类人就叫做 prompt engineer，专门负责设计 prompt，让 AI 回答的更好。我是强烈支持这一点的，一个好的 prompt 可以让 AI 的回答更加专业，更加有逻辑性。

OpenAI 发展至今，已经可以在移动中断上方便的对话，Android，iOS 也都是有响应的 App，但你有哦没有想过一个场景，假如你在油管上看到了一个视频，但是你现在没时间看，希望 AI 可以帮你看，实际上要完成这样的一种流的工作，还是需要一些工程化的手段的。而 fabric 就是这样一个工具，他不需要你会写专业的 prompt，感觉他的作用就是让 AI 更易用了，普通人都可以专业的用。
fabric 闪亮登场

今天的主题是 fabric，fabric是一个开源框架，它通过一系列预定义的AI提示（Patterns），帮助我们解决日常生活中的挑战。想象一下，无论是写作、学习还是工作中的任何问题，fabric都能提供定制化的AI辅助。

```shell
yt --transcript \
https://youtube.com/watch?v=uXs-zPc63kM \
| fabric --stream --pattern extract_wisdom
```

这是一个典型的使用场景，这里yt表示把 YouTube 视频转为文本，然后链接到 fabric，fabric 就像一个路由器，将流分发到了 extract_wisdom ，这样我们就可以在不看视频的情况下，获取到视频中的精华内容。

当然，fabric 中还有很多其他的模式，如图所示：
图片

这些模式都是使用 markdown 的，所以你可以很方便地按照自己的需求定义自己的模式。这些模式将会通过内部的连接，可以很方便地串联起来，形成一个完整的流程，帮你执行一个可能需要多个工序的任务，这妥妥的就是你的私人秘书。想象空间很大，总之，将你的日常的工作整理为 patterns，然后你一个启动命令，后面一连串的动作就像多米勒骨牌效应那样，直到完成，想想就觉得无比舒适！
命令行直接玩

你以为这样就完了吗，不，fabric 还将这样的一些能力，封装成了命令行工具，你可以直接在命令行中使用，结合 linux 的管道，你可以做到很多有意思的事情，真的很有想象空间。

比如：

```shell
echo "An idea that coding is like speaking with rules." \
| write_essay
```

试着思考下，这意味着什么，这意味着 AI 对你来讲，就是透明的，所以，我一直在想一个问题，AI 未来会不会成为一个基础设施，就像电、水、气一样，无处不在，但是又无需我们关心。fabric 的出现，让我对这个问题有了更多的思考，或许，这是 AI 潜移默化地改变我们的生活方式。
fabric 的未来

目前 11k 的 star，绝对的扛把子，持续关注，未来可期。地址：https://github.com/danielmiessler/fabric

5 月 23 号，fabric 官网宣称会从 Python 转向 Go，这样一来，fabric更加容易安装， 而且性能会得到很大的提升，这也意味着我们使用它会更加方便。