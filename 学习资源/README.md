# 学习资源

## [LangChain](LangChain/README.md)
- [LangChain基本概念入门](LangChain/LangChain%E5%9F%BA%E6%9C%AC%E6%A6%82%E5%BF%B5%E5%85%A5%E9%97%A8.md)
- [LangServe全面使用指南](LangChain/LangServe%E5%85%A8%E9%9D%A2%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97.md)
## [独立开发](独立开发/README.md)
- [一个极好的独立开发者资源合集](%E7%8B%AC%E7%AB%8B%E5%BC%80%E5%8F%91/%E4%B8%80%E4%B8%AA%E6%9E%81%E5%A5%BD%E7%9A%84%E7%8B%AC%E7%AB%8B%E5%BC%80%E5%8F%91%E8%80%85%E8%B5%84%E6%BA%90%E5%90%88%E9%9B%86.md)