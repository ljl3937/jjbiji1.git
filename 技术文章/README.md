# 技术文章

## [AI动态](AI动态/README.md)
- [全球首位AI程序员Devin的问世：编程领域的未来已来](AI%E5%8A%A8%E6%80%81/%E5%85%A8%E7%90%83%E9%A6%96%E4%BD%8DAI%E7%A8%8B%E5%BA%8F%E5%91%98Devin%E7%9A%84%E9%97%AE%E4%B8%96%EF%BC%9A%E7%BC%96%E7%A8%8B%E9%A2%86%E5%9F%9F%E7%9A%84%E6%9C%AA%E6%9D%A5%E5%B7%B2%E6%9D%A5.md)
- [kimi终于增加智能体功能了](AI%E5%8A%A8%E6%80%81/kimi%E7%BB%88%E4%BA%8E%E5%A2%9E%E5%8A%A0%E6%99%BA%E8%83%BD%E4%BD%93%E5%8A%9F%E8%83%BD%E4%BA%86.md)
- [震撼发布！Meta Llama 3惊艳登场](AI%E5%8A%A8%E6%80%81/%E9%9C%87%E6%92%BC%E5%8F%91%E5%B8%83%EF%BC%81Meta%20Llama%203%E6%83%8A%E8%89%B3%E7%99%BB%E5%9C%BA.md)
## [模型设计](模型设计/README.md)
- [Agent工作流的四种关键设计思维模式](%E6%A8%A1%E5%9E%8B%E8%AE%BE%E8%AE%A1/Agent%E5%B7%A5%E4%BD%9C%E6%B5%81%E7%9A%84%E5%9B%9B%E7%A7%8D%E5%85%B3%E9%94%AE%E8%AE%BE%E8%AE%A1%E6%80%9D%E7%BB%B4%E6%A8%A1%E5%BC%8F.md)
- [AI时代下的思维革新：底层思维与顶层框架](%E6%A8%A1%E5%9E%8B%E8%AE%BE%E8%AE%A1/AI%E6%97%B6%E4%BB%A3%E4%B8%8B%E7%9A%84%E6%80%9D%E7%BB%B4%E9%9D%A9%E6%96%B0%EF%BC%9A%E5%BA%95%E5%B1%82%E6%80%9D%E7%BB%B4%E4%B8%8E%E9%A1%B6%E5%B1%82%E6%A1%86%E6%9E%B6.md)
- [dify、coze、streamlit、chainlit对比](%E6%A8%A1%E5%9E%8B%E8%AE%BE%E8%AE%A1/dify%E3%80%81coze%E3%80%81streamlit%E3%80%81chainlit%E5%AF%B9%E6%AF%94.md)