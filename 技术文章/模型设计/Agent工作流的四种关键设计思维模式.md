# Agent工作流的四种关键设计思维模式

今天从提示词偶像小七姐的知识星球中摘抄的一个信息，分享给大家。对研究Agent工作流的同学有帮助。

![20240420115026](https://jjbiji-pic.oss-cn-beijing.aliyuncs.com/pic20240420115026.png)

AI Agent工作流的四种关键设计模式

斯坦福大学教授、人工智能领域的知名学者吴恩达近期提出了一个关于 AI Agent 工作流的前瞻性观点。他预测，这种工作流在今年内将为人工智能的发展带来重大突破，其影响力有可能超越下一代基础模型。

吴恩达教授强调了这一趋势的重要性，并倡导人工智能领域的专业人士们密切关注AI Agent工作流的进展。在他的论述中，详细阐述了AI Agent工作流的四种关键设计模式：

* Reflection（反思）
* Tool use（工具使用）
* Planning（规划）
* Multi-agent collaboration（多智能体协作）
  
这些模式共同构成了AI Agent工作流的核心架构。

我们用这四篇飞书翻译、梳理了数篇相关论文的研究成果，通过下面这4篇文章对这四种模式进行深入解读，希望为大家提供一个关于当前 AI Agent 工作流研究动态的概览：

导览页：

[AI Agent工作流的四种关键设计模式](https://vxc3hj17dym.feishu.cn/wiki/E1xIweljbijtCDkDC9BcLSIGn9f)

单篇链接：

[Reflection：模型自我评估与完善的3种框架](https://vxc3hj17dym.feishu.cn/wiki/HFRewaEnUix7vqkOuDTcwPPnn4b)

[Tool use：有效地利用外部工具](https://vxc3hj17dym.feishu.cn/wiki/Qb2FwhyOBiw3pyk1PDAc9Bj7nKh)

[Planning：利用LLM规划任务步骤](https://vxc3hj17dym.feishu.cn/wiki/H3MswdK18ipNhXkeyyBcm3gPnsg)

[Multi-agent collaboration 多智能体协作](https://vxc3hj17dym.feishu.cn/wiki/VG0Bwa38SiJRkVkCRgMcpLg6nzh)
