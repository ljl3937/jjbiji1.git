# 模型设计

- [Agent工作流的四种关键设计思维模式](Agent%E5%B7%A5%E4%BD%9C%E6%B5%81%E7%9A%84%E5%9B%9B%E7%A7%8D%E5%85%B3%E9%94%AE%E8%AE%BE%E8%AE%A1%E6%80%9D%E7%BB%B4%E6%A8%A1%E5%BC%8F.md)
- [AI时代下的思维革新：底层思维与顶层框架](AI%E6%97%B6%E4%BB%A3%E4%B8%8B%E7%9A%84%E6%80%9D%E7%BB%B4%E9%9D%A9%E6%96%B0%EF%BC%9A%E5%BA%95%E5%B1%82%E6%80%9D%E7%BB%B4%E4%B8%8E%E9%A1%B6%E5%B1%82%E6%A1%86%E6%9E%B6.md)
- [dify、coze、streamlit、chainlit对比](dify%E3%80%81coze%E3%80%81streamlit%E3%80%81chainlit%E5%AF%B9%E6%AF%94.md)