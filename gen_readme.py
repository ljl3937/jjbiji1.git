import os
from urllib.parse import quote

def url_encode(url):
    """
    对 URL 中的特殊字符进行编码。
    
    参数:
    url -- 需要编码的原始 URL 字符串
    
    返回:
    编码后的 URL 字符串
    """
    # 对 URL 进行编码
    encoded_url = quote(url, safe=':/?#')
    return encoded_url
def create_readme(directory, root_directory='.'):
    # 获取目录下的所有文件和文件夹
    entries = os.listdir(directory)
    
    # 排除隐藏文件和README.md文件
    entries = [entry for entry in entries if entry not in [".vuepress", "node_modules"]]
    
    # 创建或更新README.md文件
    readme_path = os.path.join(directory, 'README.md')
    
    # 如果当前目录不是根目录，则创建或更新README.md文件
    if directory != root_directory:
        with open(readme_path, 'w') as readme:
            # 写入目录名作为标题
            readme.write(f'# {os.path.basename(directory)}\n')
            
            # 遍历目录下的所有文件和文件夹
            for entry in entries:
                entry_path = os.path.join(directory, entry)
                if os.path.isdir(entry_path):
                    # 如果是目录，则先写入目录下的文件链接，再递归调用create_readme
                    create_readme(entry_path, root_directory)
                    # 在README.md中添加目录链接
                    readme.write(f'\n## [{entry}]({entry}/README.md)')
                    for root, dirs, files in os.walk(entry_path):
                        for file in files:
                            if file != 'README.md':
                                link = url_encode(f'{entry}/{file}')
                                read_str = file.split('.')[0]
                                readme.write(f'\n- [{read_str}]({link})')
                else:
                    # 如果是文件，则在README.md中添加文件链接
                    print(entry)
                    if entry != 'README.md':
                        link = url_encode(entry)
                        read_str = entry.split('.')[0]
                        readme.write(f'\n- [{read_str}]({link})')
    else:
        # 如果当前目录是根目录，则遍历目录，在子目录中创建或更新README.md文件
        for entry in entries:
            entry_path = os.path.join(directory, entry)
            if os.path.isdir(entry_path):
                create_readme(entry_path, root_directory)

# 从当前目录开始递归创建或更新README.md文件
create_readme('.')